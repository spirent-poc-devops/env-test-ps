#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

###################################################################
# Prepare hosts file
$ansibleInventory = @("[master_single]")
$ansibleInventory += "master ansible_host=$(Get-EnvMapValue -Map $resources -Key "k8s.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "k8s.username") ansible_ssh_private_key_file=temp/keypair_k8s_$(Get-EnvMapValue -Map $config -Key "environment.prefix")/secrets/$(Get-EnvMapValue -Map $resources -Key "k8s.keypair_name").pem"

Set-Content -Path "$PSScriptRoot/../temp/k8s_ansible_hosts" -Value $ansibleInventory

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
}

# Install k8s dependecies
$k8s_kubelet_version = Get-EnvMapValue -Map $config -Key "k8s.kubelet_version"
$k8s_kubeadm_version = Get-EnvMapValue -Map $config -Key "k8s.kubeadm_version"
$k8s_kubectl_version = Get-EnvMapValue -Map $config -Key "k8s.kubectl_version"

$templateParams = @{ 
    k8s_kubelet_version=$k8s_kubelet_version
    k8s_kubeadm_version=$k8s_kubeadm_version
    k8s_kubectl_version=$k8s_kubectl_version
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/k8s_prerequisites_playbook.yml" -OutputPath "$PSScriptRoot/../temp/k8s_prerequisites_playbook.yml" -Params1 $templateParams
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_prerequisites_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_prerequisites_playbook.yml"
}

# Single master cluster
$env_prefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$k8s_network = Get-EnvMapValue -Map $config -Key "k8s.network"
$k8s_username = Get-EnvMapValue -Map $config -Key "k8s.username"

$templateParams = @{ 
    env_prefix = $env_prefix
    k8s_network = $k8s_network;
    k8s_username = $k8s_username;
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/k8s_master_single_playbook.yml" -OutputPath "$PSScriptRoot/../temp/k8s_master_single_playbook.yml" -Params1 $templateParams
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_single_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_master_single_playbook.yml"
}

if ($LastExitCode -ne 0) {
    Set-EnvMapValue -Map $resources -Key "k8s.state" -Value "install failed"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
    Write-EnvError -Component "kubernetes" "Error on installing k8s cluster. See logs above."
}

# Replace k8s cluster name in new kube config
(Get-Content -Path "/home/$k8s_username/.kube/config_$env_prefix" -Raw)  -replace "kubernetes-admin@kubernetes","$env_prefix-k8s" -replace "kubernetes","$env_prefix-k8s" | Set-Content "/home/$k8s_username/.kube/config_$env_prefix"

if (!(Test-Path -Path "/home/$k8s_username/.kube/config") -or [String]::IsNullOrWhiteSpace((Get-content "/home/$k8s_username/.kube/config"))) {
    # First k8s cluster created from mgmt station. Just copy config
    Get-Content -Path "/home/$k8s_username/.kube/config_$env_prefix" | Add-Content -Path "/home/$k8s_username/.kube/config"
} else {
    # Verify that cluster not allready in .kube/config (in case of update)
    $clusters = kubectl config get-contexts -o name
    if ($clusters -NotContains "$env_prefix-k8s") {
        # Add new cluster to mgmt station kube/config
        $mgmtConfigText = Get-Content "/home/$k8s_username/.kube/config"
        $newEnvConfigText = Get-Content "/home/$k8s_username/.kube/config_$env_prefix"

        $newMgmtConfigText = @() 

        # indexes are default for kubeadm configs
        $newCluserSection = $newEnvConfigText[2..5]
        $newContextSection = $newEnvConfigText[7..10]
        $newUserSection = $newEnvConfigText[15..18]

        foreach ($Line in $mgmtConfigText) {    
            $newMgmtConfigText += $Line

            if ($Line -match "clusters:") {
                $newMgmtConfigText += $newCluserSection
            } 
            if ($Line -match "contexts:") {
                $newMgmtConfigText += $newContextSection
            } 
            if ($Line -match "users:") {
                $newMgmtConfigText += $newUserSection
            } 
        }

        Set-Content "/home/$k8s_username/.kube/config" $newMgmtConfigText
    }
}

# Write k8s resources
Set-EnvMapValue -Map $resources -Key "k8s.type" -Value "kubeadm"
Set-EnvMapValue -Map $resources -Key "k8s.state" -Value "installed"
Set-EnvMapValue -Map $resources -Key "k8s.inventory" -Value $ansibleInventory
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################


###################################################################
# Set variables from config
$k8s_namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$templateParams = @{ k8s_namespace=$k8s_namespace }
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/namespace.yml" -OutputPath "$PSScriptRoot/../temp/namespace.yml" -Params1 $templateParams
# Create k8s namespace
kubectl apply -f "$PSScriptRoot/../temp/namespace.yml"

# Set variables from config
$k8s_namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$k8s_blobs_storage_gb = Get-EnvMapValue -Map $config -Key "k8s.blobs_storage_gb"

$templateParams = @{ 
    k8s_namespace=$k8s_namespace ; 
    k8s_blobs_storage_gb=$k8s_blobs_storage_gb 
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/blobs_pv.yml" -OutputPath "$PSScriptRoot/../temp/blobs_pv.yml" -Params1 $templateParams
# Create k8s blobs persistent volume
kubectl apply -f "$PSScriptRoot/../temp/blobs_pv.yml"
kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=release-1.4"
kubectl apply -f "$PSScriptRoot/templates/aws-sc-ebs.yml"
# Write k8s resources
Set-EnvMapValue -Map $resources -Key "k8s.namespace" -Value (Get-EnvMapValue -Map $config -Key "k8s.namespace")
Set-EnvMapValue -Map $resources -Key "k8s.blobs_storage_gb" -Value (Get-EnvMapValue -Map $config -Key "k8s.blobs_storage_gb")
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################
