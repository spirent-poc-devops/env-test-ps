#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare hosts file if missing
if (!(Test-Path -Path "$PSScriptRoot/../temp/k8s_ansible_hosts")) {
    $ansibleInventory = @("[master_single]")
    $ansibleInventory += "master ansible_host=$(Get-EnvMapValue -Map $resources -Key "k8s.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "k8s.username") ansible_ssh_private_key_file=temp/keypair_k8s_$(Get-EnvMapValue -Map $config -Key "environment.prefix")/secrets/$(Get-EnvMapValue -Map $resources -Key "k8s.keypair_name").pem"

    Set-Content -Path "$PSScriptRoot/../temp/k8s_ansible_hosts" -Value $ansibleInventory
}

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
}

# Destroy k8s cluster
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/k8s_uninstall_playbook.yml" -OutputPath "$PSScriptRoot/../temp/k8s_uninstall_playbook.yml" -Params1 $templateParams
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_uninstall_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/k8s_ansible_hosts" "$PSScriptRoot/../temp/k8s_uninstall_playbook.yml"
}

# Remove k8s cluster from mgmt station .kube/config
$envPrefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$k8sUsername = Get-EnvMapValue -Map $config -Key "k8s.username"

# Remove k8s cluster from config
if (Test-Path "/home/$k8sUsername/.kube/config") {
    Write-EnvInfo -Component "kubernetes" "Removing $envPrefix k8s cluster from mgmt station .kube/config"

    kubectl config unset "users.$envPrefix-k8s-admin"
    kubectl config unset "contexts.$envPrefix-k8s"
    kubectl config unset "clusters.$envPrefix-k8s"
    $mgmtConfigText = Get-Content "/home/$k8sUsername/.kube/config"
    $mgmtConfigText -replace "null","" | Set-Content "/home/$k8sUsername/.kube/config"

    # Remove env config
    Remove-Item -Path "/home/$k8sUsername/.kube/config_$envPrefix" -Force
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "k8s") {
    Remove-EnvMapValue -Map $resources -Key "k8s.type"
    Remove-EnvMapValue -Map $resources -Key "k8s.state"
    Remove-EnvMapValue -Map $resources -Key "k8s.inventory"
    Remove-EnvMapValue -Map $resources -Key "k8s.blobs_storage_gb"
    Remove-EnvMapValue -Map $resources -Key "k8s.namespace"
}
# Delete redis from resource file
if (Test-EnvMapValue -Map $resources -Key "redis") {
    Remove-EnvMapValue -Map $resources -Key "redis.port"
    Remove-EnvMapValue -Map $resources -Key "redis.host"
    Remove-EnvMapValue -Map $resources -Key "redis.endpoint"
}
# Delete logging from resource file
if (Test-EnvMapValue -Map $resources -Key "logging") {
    Remove-EnvMapValue -Map $resources -Key "logging.port"
    Remove-EnvMapValue -Map $resources -Key "logging.host"
    Remove-EnvMapValue -Map $resources -Key "logging.endpoint"
}
# Delete metrics from resource file
if (Test-EnvMapValue -Map $resources -Key "metrics") {
    Remove-EnvMapValue -Map $resources -Key "metrics.port"
    Remove-EnvMapValue -Map $resources -Key "metrics.host"
    Remove-EnvMapValue -Map $resources -Key "metrics.endpoint"
}
# Delete neo4j from resource file
if (Test-EnvMapValue -Map $resources -Key "neo4j") {
    Remove-EnvMapValue -Map $resources -Key "neo4j.port"
    Remove-EnvMapValue -Map $resources -Key "neo4j.bolt_port"
    Remove-EnvMapValue -Map $resources -Key "neo4j.host"
    Remove-EnvMapValue -Map $resources -Key "neo4j.endpoint"
}
# Delete redis from resource file
if (Test-EnvMapValue -Map $resources -Key "redis") {
    Remove-EnvMapValue -Map $resources -Key "redis.port"
    Remove-EnvMapValue -Map $resources -Key "redis.host"
    Remove-EnvMapValue -Map $resources -Key "redis.endpoint"
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
