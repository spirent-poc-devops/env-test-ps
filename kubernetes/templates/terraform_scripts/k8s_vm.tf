variable "k8s_instance_ami" {
  type        = string
  description = "Kubernetes vm instance ami"
}

variable "k8s_instance_type" {
  type        = string
  description = "Kubernetes vm instance type"
}

variable "k8s_keypair_name" {
  type        = string
  description = "Kubernetes vm instance keypair name"
}

variable "k8s_associate_public_ip_address" {
  type        = string
  description = "Define is kubernetes vm instance have public ip [true/false]"
}

variable "k8s_subnet_id" {
  type        = string
  description = "Kubernetes vm subnet id"
}

variable "k8s_volume_size" {
  type        = string
  description = "Kubernetes vm instance volume size"
}

variable "env_nameprefix" {
  type        = string
  description = "Environment prefix used for naming resources"
}

variable "aws_vpc" {
  type        = string
  description = "Kubernetes vm AWS vpc"
}

variable "mgmt_subnet_cidr" {
  type        = string
  description = "Management station CIDR"
}

variable "mgmt_public_ip" {
  type        = string
  description = "Management station public ip"
}

variable "public_hosted_zone_id" {
  type        = string
  description = "Hosted zone ID of the public domain"
}

variable "dns_public_name" {
  type        = string
  description = "Dns name to be used for external URLs"
}

variable "k8s_ebs_snapshot_id" {
  type        = string
  description = "Snapshot id of AMI's EBS"
}

# Configure AWS instance
resource "aws_instance" "k8s" {
  ami                         = var.k8s_instance_ami
  instance_type               = var.k8s_instance_type
  key_name                    = var.k8s_keypair_name
  associate_public_ip_address = var.k8s_associate_public_ip_address
  subnet_id                   = var.k8s_subnet_id
  vpc_security_group_ids      = [aws_security_group.k8s.id]
  iam_instance_profile = aws_iam_instance_profile.nodes.id
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = var.k8s_volume_size
    snapshot_id = var.k8s_ebs_snapshot_id
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_k8s"
  }
}

# Configure AWS security group
resource "aws_security_group" "k8s" {
  name        = "${var.env_nameprefix}_k8s"
  description = "k8s access"
  vpc_id      = var.aws_vpc
  ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.mgmt_subnet_cidr}","${var.mgmt_public_ip}/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_k8s_sg"
  }
}


resource "aws_iam_role" "nodes" {
  name = "${var.env_nameprefix}-role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "workers_AmazonEKSWorkerNodePolicy" {
  policy_arn = aws_iam_policy.ebs-cni-policy.arn
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_policy" "ebs-cni-policy" {
  name        = "K8sEBSCniPolicy_${var.env_nameprefix}"
  path        = "/"
  description = "PolicyToEnableEBSVolumes"
  policy      = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "ec2:CreateSnapshot",
          "ec2:AttachVolume",
          "ec2:DetachVolume",
          "ec2:ModifyVolume",
          "ec2:DescribeAvailabilityZones",
          "ec2:DescribeInstances",
          "ec2:DescribeSnapshots",
          "ec2:DescribeTags",
          "ec2:DescribeVolumes",
          "ec2:DescribeVolumesModifications"
        ],
        "Resource": "*"
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:CreateTags"
        ],
        "Resource": [
          "arn:aws:ec2:*:*:volume/*",
          "arn:aws:ec2:*:*:snapshot/*"
        ],
        "Condition": {
          "StringEquals": {
            "ec2:CreateAction": [
              "CreateVolume",
              "CreateSnapshot"
            ]
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:DeleteTags"
        ],
        "Resource": [
          "arn:aws:ec2:*:*:volume/*",
          "arn:aws:ec2:*:*:snapshot/*"
        ]
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:CreateVolume"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "aws:RequestTag/ebs.csi.aws.com/cluster": "true"
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:CreateVolume"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "aws:RequestTag/CSIVolumeName": "*"
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:CreateVolume"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "aws:RequestTag/kubernetes.io/cluster/*": "owned"
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:DeleteVolume"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "ec2:ResourceTag/ebs.csi.aws.com/cluster": "true"
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:DeleteVolume"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "ec2:ResourceTag/CSIVolumeName": "*"
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:DeleteVolume"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "ec2:ResourceTag/kubernetes.io/cluster/*": "owned"
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:DeleteSnapshot"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "ec2:ResourceTag/CSIVolumeSnapshotName": "*"
          }
        }
      },
      {
        "Effect": "Allow",
        "Action": [
          "ec2:DeleteSnapshot"
        ],
        "Resource": "*",
        "Condition": {
          "StringLike": {
            "ec2:ResourceTag/ebs.csi.aws.com/cluster": "true"
          }
        }
      }
    ]
  }
    EOF

}

resource "aws_iam_instance_profile" "nodes" {
  name = "nodes-instance-profile-${var.env_nameprefix}"
  role = aws_iam_role.nodes.name
  path = "/"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "env_ingress" {
  depends_on = [aws_instance.k8s]
  zone_id = var.public_hosted_zone_id
  name    = var.dns_public_name
  type    = "A"
  ttl = "3600"
  records = [aws_instance.k8s.private_ip]


}


# Save k8s instance id
output "k8s_id" {
  value       = aws_instance.k8s.id
  description = "The unique identifier of the k8s virtual machine."
}

# Save k8s security group id
output "k8s_sg_id" {
  value       = aws_security_group.k8s.id
  description = "The unique identifier of the k8s security group."
}

# Save k8s instance private ip
output "k8s_private_ip" {
  value       = aws_instance.k8s.private_ip
  description = "The private IP address of the k8s virtual machine."
}

# Save k8s instance public ip
output "k8s_public_ip" {
  value       = aws_instance.k8s.public_ip
  description = "The public IP address of the k8s virtual machine."
}
