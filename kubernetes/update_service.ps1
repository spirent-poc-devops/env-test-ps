#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $false, Position = 1)]
    [string] $ResourcePath
)


# Delegate execution to idempotent create script
. "$PSScriptRoot/update_resources.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath

# . "$PSScriptRoot/delete.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath

#  Delegate execution to idempotent create script
. "$PSScriptRoot/create.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath
