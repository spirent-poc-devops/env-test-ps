#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Alias("r", "Resources")]
    [Parameter(Mandatory = $false, Position = 1)]
    [string] $ResourcePath,

    [Alias("c")]
    [Parameter(Mandatory = $false, Position = 2)]
    [string[]] $Components
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq "")) {
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}

# Read config 
$config = Read-EnvConfig -ConfigPath $ConfigPath

# Switch to k8s cluster from config
$envPrefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
Switch-EnvKubeContext "$envPrefix-k8s"

if ($Components -eq $null) {
    $Components = @(
        "kubernetes",
        "kafka",
        "timescale",
        "redis",
        "neo4j",
        "logging",
        "metrics",
        "apisix",
        "connections",
        "environment"
    )
} else {
    # required for pwsh on ubuntu
    $Components = $Components.Split(",")
}

try {
    $totalStartTime = $(Get-Date)
    $totalStatus = "SUCCESS"
    $statuses = @()

    # Clear errors variable for clean logging
    $error.clear()

    # Create components in the order they defined
    foreach ($component in $components) {    
        try {
            $startTime = $(Get-Date)
            $status = "SUCCESS"

            Write-EnvInfo -Component $component "Started updating $component component." -Delimiter

            . "$rootPath/$component/update.ps1" -ConfigPath "$ConfigPath" -ResourcePath "$ResourcePath"

            Write-EnvInfo -Component $component "Completed updating $component component." -Delimiter
        } 
        # Catch and log any errors
        catch {
            $status = "FAIL"
            $totalStatus = "FAIL"

            $error
            Write-EnvError -Component $component "Can't update the $component component. See logs above."
        }
        finally {
            $elapsedTime = $(Get-Date) - $startTime
            $duration = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
            $statuses += @{ Component=$component; Duration=$duration; Status=$status }

            if ($status -eq "FAIL") {
                Write-EnvError -Component $Component "$component update $status in $duration (HH:mm:ss.ms)"
            }
            else {
                Write-EnvInfo -Component $Component "$component update $status in $duration (HH:mm:ss.ms)" -Color "Yellow"
            }
        }
    }
}
finally {
    $totalElapsedTime = $(Get-Date) - $totalStartTime
    $totalDuration = "{0:HH:mm:ss.ffff}" -f ([datetime]$totalElapsedTime.Ticks)

    Write-EnvInfo -Component "environment" "Overall update status: $totalStatus" -Color "Yellow"

    $statuses | ForEach-Object {[PSCustomObject]$_} | Format-Table -AutoSize

    Write-EnvInfo -Component "environment" "Total update duration: $totalDuration (HH:mm:ss.ms)" -Color "Yellow"
}
