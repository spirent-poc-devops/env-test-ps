# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> Spirent VisionWorks Test Environment

### 1.0.0 (2021-08-30)

Initial release of the environment template

#### Features
* Create, update and delete scripts
* Kubernetes single-instance cluster
* Timescale single-instance configuration
* Kafka message broker running in Kubernetes
* Redis distributed cache running in Kubernetes

#### Breaking Changes
No breaking changes since this is the first version

#### Bug Fixes
No fixes in this version

