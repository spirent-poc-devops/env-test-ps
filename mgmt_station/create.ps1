#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$initialPath = Get-Location

# Check Mgmt Station Name
$envPrefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$MgmtKeypairName = "$envPrefix-mgmt-station"

# Set terraform env variables
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $terraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "mgmtstation" "Can't initialize terraform. Watch logs above or check '$terraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "mgmtstation" "Can't execute terraform plan. Watch logs above or check '$terraformPath' folder content."
}

terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "mgmtstation" "Can't create cloud resources. Watch logs above or check '$terraformPath' folder content."
}

# Save terraform state to config folder
Save-State -Component "mgmt" -EnvPrefix $envPrefix

$terraform_public_ip_output=$(terraform output -json public_ip)
$publicIP=$terraform_public_ip_output.Substring(3,$terraform_public_ip_output.length-3-3)
Write-Host "Getting public Ip:" $publicIP

$terraform_private_ip_output=$(terraform output -json private_ip)
$privateIP=$terraform_private_ip_output.Substring(3,$terraform_private_ip_output.length-3-3)
Write-Host "Getting private Ip:" $privateIP

Set-Location -Path $initialPath
Write-EnvInfo -Component "mgmtstation" "Waiting 5 minutes while mgmt station is initializing..." -Delimiter

Start-Sleep -Seconds 300
# Remove-Item .terraform -Recurse -ErrorAction Ignore

# Write AWS mgmt station resources to default resources json
Set-EnvMapValue -Map $resources -Key "mgmtstation.envprefix" -Value $envPrefix
Set-EnvMapValue -Map $resources -Key "mgmtstation.key_name" -Value $MgmtKeypairName
Set-EnvMapValue -Map $resources -Key "mgmtstation.instance_name" -Value $MgmtKeypairName
Set-EnvMapValue -Map $resources -Key "mgmtstation.public_ip" -Value $publicIP
Set-EnvMapValue -Map $resources -Key "mgmtstation.private_ip" -Value $privateIP

if (Get-EnvMapValue -Map $config -Key "mgmtstation.publicly_available") {
    $mgmtIp = $publicIP
} else {
    $mgmtIp = $privateIP
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
    
# Copy whole project to mgmt station
# Define os type by full path
if ($config.copy_project_to_mgmt_station) {

    $instanceUsername = Get-EnvMapValue -Map $config -Key "mgmtstation.instance_username"
    if($IsMacOS)
    {
        # macos
        ssh -o StrictHostKeyChecking=accept-new "$instanceUsername@$mgmtIp" -i "$terraformPath/secrets/$MgmtKeypairName.pem" `
            "mkdir -p /home/$instanceUsername/env-test-ps"
        # Get-EnvMapValue -Map $config -Key "mgmtstation.instance_type"]
        Set-Location "$PSScriptRoot/.."
        $tmp = (Get-Item -Path ".\").FullName

        Write-Host "Copying Enviornment files from local to mgmt-station"
        scp -i "$terraformPath/secrets/$MgmtKeypairName.pem" -r $tmp "$instanceUsername@$($mgmtIp):/home/$instanceUsername"
        ssh  "$instanceUsername@$mgmtIp" -i "$terraformPath/secrets/$MgmtKeypairName.pem"   "chmod +x -R /home/$instanceUsername/env-test-ps/*"
        if ($LastExitCode -ne 0) {
            Write-Error "File transfer failed. Watch logs above and make sure it doesn't exist"
        }
        Write-Host "Copying Enviornment files from local to mgmt-station is Done"
    } else {
        if ($PSScriptRoot[0] -eq "/") {
            # ubuntu
            ssh -o StrictHostKeyChecking=accept-new "$instanceUsername@$mgmtIp" -i "$terraformPath/secrets/$MgmtKeypairName.pem" `
                "mkdir -p /home/$instanceUsername/env-test-ps"

            Write-Host "Copying Enviornment files from local to mgmt-station"
            $null = scp -i "$terraformPath/secrets/$MgmtKeypairName.pem" -r "$PSScriptRoot/.." "$instanceUsername@$($mgmtIp):/home/$instanceUsername"
            Write-Host "Copying Enviornment files from local to mgmt-station is Done"
        } else {
            # Remove unneeded files that give a "Permission denied" error on Windows
            Remove-Item -Force -Recurse -ErrorAction Ignore -Path "$terraformPath/.terraform/modules/mgmt_station_instance/.git/objects/pack/*"
            Remove-Item -Force -Recurse -ErrorAction Ignore -Path "$terraformPath/.terraform/modules/mgmt_station_instance.ssh_key_pair/.git/objects/pack/*"

            # windows
            ssh -o StrictHostKeyChecking=accept-new "$instanceUsername@$mgmtIp" -i "$terraformPath/secrets/$MgmtKeypairName.pem" `
                "mkdir -p /home/$instanceUsername/env-test-ps"

            Write-Host "Copying Enviornment files from local to mgmt-station"
            $null = scp -i "$terraformPath/secrets/$MgmtKeypairName.pem" -r "$PSScriptRoot/../*" "$instanceUsername@$($mgmtIp):/home/$instanceUsername/env-test-ps"
            Write-Host "Copying Enviornment files from local to mgmt-station is Done"
        }
    }
    if ($LastExitCode -eq 0) {
        Write-Host "Creating id_rsa key"
        ssh -o StrictHostKeyChecking=accept-new "$instanceUsername@$mgmtIp" -i "$terraformPath/secrets/$MgmtKeypairName.pem" `
            "ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa <<<y >/dev/null 2>&1"

        if ($LastExitCode -eq 0) { Write-Host "Key created successfully" }
        else { Write-Host "Key creation failed - please create one manually before creating k8s cluster" }

        # Copy this project to mgmt station with configs and resources
        Write-Host "Proceed the environment creation on mgmt station:"
        Write-Host "*********Login to mgmt_station**************** "
    
        # Set-Location $PSScriptRoot/..
        $sshCommand = "ssh -i $terraformPath/secrets/$MgmtKeypairName.pem $instanceUsername@$($mgmtIp)"
        Write-Host $sshCommand -ForegroundColor Green
        Set-EnvMapValue -Map $resources -Key "mgmtstation.ssh_command" -Value $sshCommand
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
        Write-Host "*********Login to mgmt_station**************** "
    }
} else { 
    Write-EnvError -Component "mgmtstation" "Can't Copy files to Mgmt Station, see logs above"
}