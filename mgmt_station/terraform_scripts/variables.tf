variable "ssh_key_pair_path" {
  description = "Path to where the generated key pairs will be created. Defaults to $${path.cwd}"
  default     = "./secrets"
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "aws_credentials_file" {
  type        = string
  description = "AWS Credentials File"
}

variable "aws_profile" {
  type        = string
  description = "AWS Profile"
}

variable "vpc" {
  type        = string
  description = "AWS VPC ID"
}

variable "env_prefix" {
  type        = string
  description = "Environment Name Prefix"
}

variable "account_id" {
  description = "Account ID of the AWS account"
}

variable "terraform_role_name" {
  description = "AWS role used for Terraform code"
}

variable "namespace" {
  description = "Namespace (e.g. `cp` or `cloudposse`)"
}

variable "stage" {
  description = "Stage (e.g. `prod`, `dev`, `staging`"
}

variable "stage_short" {
  description = "Stage shorthand (e.g. 'p', 't', 'd')"
}

variable "delimiter" {
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  description = "Additional attributes (e.g. `1`)"
  type        = list(any)
  default     = []
}

variable "tags" {
  description = "Additional tags"
  type        = map(any)
  default     = {}
}

variable "ami" {
  description = "Additional tags"
  type        = string
}

variable "ami_owner" {
  description = "AWS account ID that owns the ami"
}

variable "root_volume_size" {
  description = "Additional tags"
  type        = string
}

variable "instance_type" {
  description = "Additional tags"
  type        = string
}

variable "subnet" {
  description = "Additional tags"
  type        = string
}

variable "sg_id" {
  description = "Additional tags"
  type        = string
}