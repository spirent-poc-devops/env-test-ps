#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Delete management station
$initialPath = Get-Location

$envPrefix = Get-EnvMapValue -Map $config -Key "environment.prefix"

if (Get-EnvMapValue -Map $resources -Key "mgmtstation.instance_name") {
    # Set terraform env variables
    . "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

    Set-Location $terraformPath
    terraform init
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "mgmtstation" "Can't initialize terraform. Watch logs above or check '$terraformPath' folder content."
    }

    terraform plan
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "mgmtstation" "Can't execute terraform plan. Watch logs above or check '$terraformPath' folder content."
    }

    terraform  destroy -auto-approve
    # Check for error
    if ($LastExitCode -ne 0) {
        Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "mgmtstation" "Can't create cloud resources. Watch logs above or check '$terraformPath' folder content."
    }

    # Delete terraform state from config folder
    Remove-State -Component "mgmt" -EnvPrefix $envPrefix

    # Delete results and save resource file to disk
    Set-Location -Path $initialPath
    if (Test-EnvMapValue -Map $resources -Key "mgmtstation") {
        Remove-EnvMapValue -Map $resources -Key "mgmtstation.envprefix"
        Remove-EnvMapValue -Map $resources -Key "mgmtstation.key_name"
        Remove-EnvMapValue -Map $resources -Key "mgmtstation.instance_name"
        Remove-EnvMapValue -Map $resources -Key "mgmtstation.public_ip"
        Remove-EnvMapValue -Map $resources -Key "mgmtstation.private_ip"
        Remove-EnvMapValue -Map $resources -Key "mgmtstation.ssh_command"
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
    }

} else {
    Write-EnvError -Component "mgmtstation" "Mgmt Staion is not present,proceeding to next step"
    exit 0 
}
