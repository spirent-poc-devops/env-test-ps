#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$environmentType = Get-EnvMapValue -Map $config -Key "environment.type"
$environmentVersion = Get-EnvMapValue -Map $config -Key "environment.version"
$environmentTime = Get-Date -Format "o"

# Set-EnvMapValue -Map $resources -Key "environment.type" -Value $environmentType
# Set-EnvMapValue -Map $resources -Key "environment.version" -Value $environmentVersion
Set-EnvMapValue -Map $resources -Key "environment.delete_time" -Value $environmentTime
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

# Write to config map
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
kubectl delete --ignore-not-found=true configmap environment -n $namespace | Out-Null
