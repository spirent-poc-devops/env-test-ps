#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare hosts file if missing
if (!(Test-Path -Path "$PSScriptRoot/../temp/kafka_ansible_hosts")) {
    $ansibleInventory = @("[kafka]")
    $ansibleInventory += "kafka_0 ansible_host=$(Get-EnvMapValue -Map $resources -Key "kafka.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "kafka.username") ansible_ssh_private_key_file=temp/keypair_kafka_$(Get-EnvMapValue -Map $config -Key "environment.prefix")/secrets/$(Get-EnvMapValue -Map $resources -Key "kafka.keypair_name").pem"

    Set-Content -Path "$PSScriptRoot/../temp/kafka_ansible_hosts" -Value $ansibleInventory
}

# Destroy kafka component
$templateParams = @{}
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/kafka_uninstall_playbook.yml" -OutputPath "$PSScriptRoot/../temp/kafka_uninstall_playbook.yml" -Params1 $templateParams
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/kafka_ansible_hosts" "$PSScriptRoot/../temp/kafka_uninstall_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/kafka_ansible_hosts" "$PSScriptRoot/../temp/kafka_uninstall_playbook.yml"
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "kafka") {
    Remove-EnvMapValue -Map $resources -Key "kafka.endpoint"
    Remove-EnvMapValue -Map $resources -Key "kafka.inventory"
    Remove-EnvMapValue -Map $resources -Key "kafka.host"
    Remove-EnvMapValue -Map $resources -Key "kafka.port"
    Remove-EnvMapValue -Map $resources -Key "kafka.username"
    Remove-EnvMapValue -Map $resources -Key "kafka.zookeeper_version"
    Remove-EnvMapValue -Map $resources -Key "kafka.kafka_version"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
