#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

################################## kafka Virtual Machine ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws kafka resources
Set-Location $kafkaTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't initialize terraform. Watch logs above or check '$kafkaTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't execute terraform plan. Watch logs above or check '$kafkaTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't delete cloud resources. Watch logs above or check '$kafkaTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "kafka") {
    Remove-EnvMapValue -Map $resources -Key "kafka.private_ip"
    Remove-EnvMapValue -Map $resources -Key "kafka.public_ip"
    Remove-EnvMapValue -Map $resources -Key "kafka.id"
    Remove-EnvMapValue -Map $resources -Key "kafka.sg_id"
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################

################################## Keypair ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $keypairTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't delete cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "keypair_kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk

if (Test-EnvMapValue -Map $resources -Key "kafka") {
    Remove-EnvMapValue -Map $resources -Key "kafka.keypair_name"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
###################################################################
