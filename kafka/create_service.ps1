#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare hosts file
$ansibleInventory = @("[kafka]")
$ansibleInventory += "kafka_0 ansible_host=$(Get-EnvMapValue -Map $resources -Key "kafka.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "kafka.username") ansible_ssh_private_key_file=temp/keypair_kafka_$(Get-EnvMapValue -Map $config -Key "environment.prefix")/secrets/$(Get-EnvMapValue -Map $resources -Key "kafka.keypair_name").pem"

Set-Content -Path "$PSScriptRoot/../temp/kafka_ansible_hosts" -Value $ansibleInventory

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/kafka_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/kafka_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
}

# Install kafka component
$instance_username = Get-EnvMapValue -Map $config -Key "kafka.username"
$zookeeper_version = Get-EnvMapValue -Map $config -Key "kafka.zookeeper_version"
$kafka_version = Get-EnvMapValue -Map $config -Key "kafka.kafka_version"

$templateParams = @{
    instance_username = $instance_username
    zookeeper_version=$zookeeper_version
    kafka_version=$kafka_version
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/kafka_install_playbook.yml" -OutputPath "$PSScriptRoot/../temp/kafka_install_playbook.yml" -Params1 $templateParams
New-Item -Path "$PSScriptRoot/../temp" -Name "kafka_ansible_templates" -ItemType "directory" -Force
Copy-Item -Path "$PSScriptRoot/templates/ansible_templates/*" -Destination "$PSScriptRoot/../temp/kafka_ansible_templates/" -Recurse -Force
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/kafka_ansible_hosts" "$PSScriptRoot/../temp/kafka_install_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/kafka_ansible_hosts" "$PSScriptRoot/../temp/kafka_install_playbook.yml"
}

if ($LastExitCode -ne 0) {
    Write-EnvError -Component "kafka" "Error on installing kafka. See logs above."
}

# Write kafka resources
$port = "9092"
$ip = (Get-EnvMapValue -Map $resources -Key "kafka.private_ip")
$endpoint = "$($ip):$($port)"
Set-EnvMapValue -Map $resources -Key "kafka.host" -Value $ip
Set-EnvMapValue -Map $resources -Key "kafka.port" -Value $port
Set-EnvMapValue -Map $resources -Key "kafka.endpoint" -Value $endpoint
Set-EnvMapValue -Map $resources -Key "kafka.inventory" -Value $ansibleInventory
Set-EnvMapValue -Map $resources -Key "kafka.username" -Value $instance_username
Set-EnvMapValue -Map $resources -Key "kafka.zookeeper_version" -Value $zookeeper_version
Set-EnvMapValue -Map $resources -Key "kafka.kafka_version" -Value $kafka_version
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
