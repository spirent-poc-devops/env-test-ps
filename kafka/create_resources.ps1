#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

############################## Keypair #####################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $keypairTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't create cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Get keypair name
$keypair=$(terraform output keypair_name).Trim('"')
Set-EnvMapValue -Map $resources -Key "kafka.keypair_name" -Value $keypair

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "keypair_kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################

################################## Kafka Virtual Machine #################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $kafkaTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't initialize terraform. Watch logs above or check '$kafkaTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't execute terraform plan. Watch logs above or check '$kafkaTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't create cloud resources. Watch logs above or check '$kafkaTerraformPath' folder content."
}

# Save results to resource file
$kafka_id=$(terraform output "kafka_id").Trim('"')
Set-EnvMapValue -Map $resources -Key "kafka.id" -Value $kafka_id
$kafka_private_ip=$(terraform output "kafka_private_ip").Trim('"') 
Set-EnvMapValue -Map $resources -Key "kafka.private_ip" -Value $kafka_private_ip
$kafka_public_ip=$(terraform output "kafka_public_ip").Trim('"')
Set-EnvMapValue -Map $resources -Key "kafka.public_ip" -Value $kafka_public_ip
$kafka_sg_id=$(terraform output "kafka_sg_id").Trim('"')
Set-EnvMapValue -Map $resources -Key "kafka.sg_id" -Value $kafka_sg_id

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################
