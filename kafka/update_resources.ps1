#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

#  Delegate execution to idempotent create script
. "$PSScriptRoot/create_resources.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath
