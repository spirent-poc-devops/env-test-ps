variable "kafka_instance_ami" {
  type        = string
  description = "Kafka vm instance ami"
}

variable "kafka_instance_type" {
  type        = string
  description = "Kafka vm instance type"
}

variable "kafka_keypair_name" {
  type        = string
  description = "Kafka vm instance keypair name"
}

variable "kafka_associate_public_ip_address" {
  type        = string
  description = "Define is Kafka vm instance have public ip [true/false]"
}

variable "kafka_subnet_id" {
  type        = string
  description = "Kafka vm subnet id"
}

variable "kafka_volume_size" {
  type        = string
  description = "Kafka vm instance volume size"
}

variable "env_nameprefix" {
  type        = string
  description = "Environment prefix used for naming resources"
}

variable "aws_vpc" {
  type        = string
  description = "Kafka vm AWS vpc"
}

variable "kafka_ebs_snapshot_id" {
  type        = string
  description = "Snapshot id of AMI's EBS"
}

variable "mgmt_subnet_cidr" {
  type        = string
  description = "Management station CIDR"
}

# Configure AWS instance
resource "aws_instance" "kafka" {
  ami                         = var.kafka_instance_ami
  instance_type               = var.kafka_instance_type
  key_name                    = var.kafka_keypair_name
  associate_public_ip_address = var.kafka_associate_public_ip_address
  subnet_id                   = var.kafka_subnet_id
  vpc_security_group_ids      = [aws_security_group.kafka.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = var.kafka_volume_size
    snapshot_id = var.kafka_ebs_snapshot_id
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_kafka"
  }
}

# Configure AWS security group
resource "aws_security_group" "kafka" {
  name        = "${var.env_nameprefix}_kafka"
  description = "kafka access"
  vpc_id      = var.aws_vpc

  ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.mgmt_subnet_cidr]
  }
  
  ingress {
    description = "access_to_kafka"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "access_to_zookeeper"
    from_port   = 2181
    to_port     = 2181
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_kafka_sg"
  }
}

# Save kafka instance id
output "kafka_id" {
  value       = aws_instance.kafka.id
  description = "The unique identifier of the kafka virtual machine."
}

# Save kafka security group id
output "kafka_sg_id" {
  value       = aws_security_group.kafka.id
  description = "The unique identifier of the kafka security group."
}

# Save kafka instance private ip
output "kafka_private_ip" {
  value       = aws_instance.kafka.private_ip
  description = "The private IP address of the kafka virtual machine."
}

# Save kafka instance public ip
output "kafka_public_ip" {
  value       = aws_instance.kafka.public_ip
  description = "The public IP address of the kafka virtual machine."
}
