variable "aws_namespace" {
  type        = string
  description = "AWS namespace"
}

variable "aws_stage" {
  type        = string
  description = "AWS stage"
}

variable "env_nameprefix" {
  type        = string
  description = "Environment prefix used for naming resources"
}

module "ssh_key_pair" {
  source = "cloudposse/key-pair/aws"
  # Cloud Posse recommends pinning every module to a specific version
  # version     = "x.x.x"
  namespace             = var.aws_namespace
  stage                 = var.aws_stage
  name                  = "${var.env_nameprefix}-kafka"
  ssh_public_key_path   = "./secrets"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
}

output "keypair_name" {
  value       = module.ssh_key_pair.key_name
  description = "The keypair name"
}