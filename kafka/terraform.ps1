#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

############################## Keypair #####################################
$keypairTerraformPath = "$PSScriptRoot/../temp/keypair_kafka_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $keypairTerraformPath)) {
    $null = New-Item $keypairTerraformPath -ItemType "directory"
} 

# Select cloud terraform templates
$env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_aws_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"

Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/provider.tf" -Destination "$keypairTerraformPath/provider.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/keypair.tf" -Destination "$keypairTerraformPath/keypair.tf"
# Load terraform state from config folder
Sync-State -Component "keypair_kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################

################################## Kafka Virtual Machine #################################
$kafkaTerraformPath = "$PSScriptRoot/../temp/kafka_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $kafkaTerraformPath)) {
    $null = New-Item $kafkaTerraformPath -ItemType "directory"
}

# Select cloud terraform templates
switch (Get-EnvMapValue -Map $config -Key "kafka.cloud") {
    "aws" {
        $env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
        $env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
        $env:TF_VAR_aws_role_name = Get-EnvMapValue -Map $config -Key "aws.terraform_role_name"
        $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
        $env:TF_VAR_kafka_instance_ami = Get-EnvMapValue -Map $config -Key "kafka.instance_ami"
        $env:TF_VAR_kafka_instance_type = Get-EnvMapValue -Map $config -Key "kafka.instance_type"
        $env:TF_VAR_kafka_keypair_name = Get-EnvMapValue -Map $resources -Key "kafka.keypair_name"
        $env:TF_VAR_kafka_subnet_id = Get-EnvMapValue -Map $config -Key "kafka.subnet_id"
        $env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
        $env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
        $env:TF_VAR_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "mgmtstation.subnet_cidr"
        $env:TF_VAR_kafka_volume_size = Get-EnvMapValue -Map $config -Key "kafka.volume_size"
        $env:TF_VAR_kafka_associate_public_ip_address = Get-EnvMapValue -Map $config -Key "kafka.associate_public_ip_address"
        $env:TF_VAR_kafka_ebs_snapshot_id = Get-EnvMapValue -Map $config -Key "kafka.ebs_snapshot_id"
        
        Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/provider.tf" -Destination "$kafkaTerraformPath/provider.tf"
        Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/kafka_vm.tf" -Destination "$kafkaTerraformPath/kafka_vm.tf"
    }
    DEFAULT {
        Write-EnvError -Component "kafka" "Cloud $(Get-EnvMapValue -Map $config -Key "kafka.cloud") doesn't supported."
    }
}
# Load terraform state from config folder
Sync-State -Component "kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################