#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Alias("r", "Resources")]
    [Parameter(Mandatory = $false, Position = 1)]
    [string] $ResourcePath
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq "")) {
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}


try{
    $totalStartTime = $(Get-Date)
    $totalStatus = "SUCCESS"
    $statuses = @()

    # Clear errors variable for clean logging
    $error.clear()

    try {
        $startTime = $(Get-Date)
        $status = "SUCCESS"

        Write-EnvInfo -Component "mgmtstation" "Started deleting mgmtstation component." -Delimiter

        . "$($rootPath)/mgmt_station/delete.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath

        Write-EnvInfo -Component "mgmtstation" "Completed deleting mgmtstation component." -Delimiter
    }
    # Catch and log any errors
    catch {
        $status = "FAIL"
        $totalStatus = "FAIL"

        $error
        Write-EnvError -Component "mgmtstation" "Can't delete the mgmtstation component. See logs above."
    }
    finally {
        $elapsedTime = $(Get-Date) - $startTime
        $duration = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
        $statuses += @{ Component="mgmtstation"; Duration=$duration; Status=$status }

        if ($status -eq "FAIL") {
            Write-EnvError -Component "mgmtstation" "mgmtstation delete $status in $duration (HH:mm:ss.ms)"
        }
        else {
            Write-EnvInfo -Component "mgmtstation" "mgmtstation delete $status in $duration (HH:mm:ss.ms)" -Color "Yellow"
        }
    }
}
finally {
    $totalElapsedTime = $(Get-Date) - $totalStartTime
    $totalDuration = "{0:HH:mm:ss.ffff}" -f ([datetime]$totalElapsedTime.Ticks)

    Write-EnvInfo -Component "environment" "Overall delete status: $totalStatus" -Color "Yellow"

    $statuses | ForEach-Object {[PSCustomObject]$_} | Format-Table -AutoSize

    Write-EnvInfo -Component "environment" "Total delete duration: $totalDuration (HH:mm:ss.ms)" -Color "Yellow"
}
