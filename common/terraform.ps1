function Save-State {
    [CmdletBinding()]
    param (
        [Alias("c")]
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Component,

        [Alias("e")]
        [Parameter(Mandatory = $true, Position = 1)]
        [string] $EnvPrefix
    )

    # Set tfstate pathes
    $tmpStatePath = "$PSScriptRoot/../temp/$Component`_$EnvPrefix/terraform.tfstate"
    $cfgStatePath = "$PSScriptRoot/../config/$EnvPrefix`_$Component`_terraform.tfstate"

    # Copy state to config folder
    if (Test-Path $tmpStatePath) {
        Copy-Item -Path $tmpStatePath -Destination $cfgStatePath
    } else {
        Write-Error "Can't find terraform state - $tmpStatePath"
    }
}

function Sync-State {
    [CmdletBinding()]
    param (
        [Alias("c")]
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Component,

        [Alias("e")]
        [Parameter(Mandatory = $true, Position = 1)]
        [string] $EnvPrefix
    )

    # Set tfstate pathes
    $tmpStatePath = "$PSScriptRoot/../temp/$Component`_$EnvPrefix/terraform.tfstate"
    $cfgStatePath = "$PSScriptRoot/../config/$EnvPrefix`_$Component`_terraform.tfstate"

    # Copy state from config folder
    if (Test-Path $cfgStatePath) {
        Copy-Item -Path $cfgStatePath -Destination $tmpStatePath
    } else {
        Write-Host "'$cfgStatePath' doesn't exists, loading terraform state skipped."
    }
}

function Remove-State {
    [CmdletBinding()]
    param (
        [Alias("c")]
        [Parameter(Mandatory = $true, Position = 0)]
        [string] $Component,

        [Alias("e")]
        [Parameter(Mandatory = $true, Position = 1)]
        [string] $EnvPrefix
    )

    # Set tfstate pathes
    $cfgStatePath = "$PSScriptRoot/../config/$EnvPrefix`_$Component`_terraform.tfstate"

    # Delete state from config folder
    Remove-Item -Path $cfgStatePath
}
