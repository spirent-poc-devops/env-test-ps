#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

# Create k8s component
kubectl apply -f "$PSScriptRoot/../temp/redis.yml"

# Record results and save them to disk
$port = kubectl get svc redis -n $k8s_namespace -o=jsonpath="{.spec.ports[0].targetPort}"
$serviceHost = "redis.$k8s_namespace.svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "redis.port" -Value $port
Set-EnvMapValue -Map $resources -Key "redis.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "redis.endpoint" -Value $endpoint
Set-EnvMapValue -Map $resources -Key "redis.replicas_count" -Value $redis_replicas_count
Set-EnvMapValue -Map $resources -Key "redis.password" -Value $redis_password
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
