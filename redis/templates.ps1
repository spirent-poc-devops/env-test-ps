#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $false, Position = 1)]
    [hashtable] $Resources
)

# Read parameters
$k8s_namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$redis_replicas_count = Get-EnvMapValue -Map $config -Key "redis.replicas_count"
$redis_password = Get-EnvMapValue -Map $config -Key "redis.password"

# Set template parameters
$templateParams = @{ 
    k8s_namespace=$k8s_namespace ; 
    redis_replicas_count=$redis_replicas_count ; 
    redis_password=$redis_password 
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/redis.yml" -OutputPath "$PSScriptRoot/../temp/redis.yml" -Params1 $templateParams
