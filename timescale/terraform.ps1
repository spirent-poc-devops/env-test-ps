#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

$env:TF_VAR_timescale_subnet_zone = Get-EnvMapValue -Map $config -Key "timescale.subnet_zone"
$env:TF_VAR_timescale_subnet_cidr = Get-EnvMapValue -Map $config -Key "timescale.subnet_cidr"
$env:TF_VAR_instance_type = Get-EnvMapValue -Map $config -Key "timescale.instance_type"
$env:TF_VAR_mgmt_ip = Get-EnvMapValue -Map $resources -Key "mgmtstation.private_ip"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_credentials_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
$env:TF_VAR_account_id = Get-EnvMapValue -Map $config -Key "aws.account_id"
$env:TF_VAR_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_stage_short = Get-EnvMapValue -Map $config -Key "aws.stage_short"
$env:TF_VAR_terraform_role_name = Get-EnvMapValue -Map $config -Key "aws.terraform_role_name"
$env:TF_VAR_ami = Get-EnvMapValue -Map $config -Key "timescale.instance_ami"
$env:TF_VAR_ami_owner = Get-EnvMapValue -Map $config -Key "timescale.instance_ami_owner"
$env:TF_VAR_subnet = Get-EnvMapValue -Map $config -Key "timescale.subnet"
$env:TF_VAR_root_volume_size = Get-EnvMapValue -Map $config -Key "timescale.root_volume_size"
$env:TF_VAR_associate_public_ip_address = Get-EnvMapValue -Map $config -Key "timescale.associate_public_ip_address"

# Initialize terraform
$terraformPath = "$PSScriptRoot/../temp/timescale_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"

if (!(Test-Path "$terraformPath")) {
    $null = New-Item "$terraformPath" -ItemType "directory"
} 

Copy-Item -Path "$PSScriptRoot/templates/aws/*.tf" -Destination $terraformPath
# Load terraform state from config folder
Sync-State -Component "timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
