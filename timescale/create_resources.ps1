#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $terraformPath

Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-Error "Can't initialize terraform. Watch logs above or check '$terraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-Error "Can't execute terraform plan. Watch logs above or check '$terraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-Error "Can't create cloud resources. Watch logs above or check '$terraformPath' folder content."
}

# Get public and private ip
$timescale_id = $(terraform output timescale_id).Trim('"')
Set-EnvMapValue -Map $resources -Key "timescale.id" -Value $timescale_id
$timescale_private_ip = $(terraform output timescale_private_ip).Trim('"')
Set-EnvMapValue -Map $resources -Key "timescale.private_ip" -Value $timescale_private_ip
$timescale_public_ip = $(terraform output timescale_public_ip).Trim('"')
Set-EnvMapValue -Map $resources -Key "timescale.public_ip" -Value $timescale_public_ip
$sg_id = $(terraform output timescale_sg_id).Trim('"')
Set-EnvMapValue -Map $resources -Key "timescale.sg_id" -Value $sg_id
$keyname = $(terraform output timescale_keypair_name).Trim('"')
Set-EnvMapValue -Map $resources -Key "timescale.keypair_name" -Value "$keyName.pem"
Set-EnvMapValue -Map $resources -Key "timescale.private_key_path" -Value "$terraformPath/secrets/$keyName.pem"

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

Write-EnvInfo -Component "timescale" "Waiting 2 minutes for timescale vm to initialize..."
Start-Sleep -Seconds 120
