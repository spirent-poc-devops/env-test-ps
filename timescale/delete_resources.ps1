#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $terraformPath

terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-Error "Can't initialize terraform. Watch logs above or check '$terraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-Error "Can't execute terraform plan. Watch logs above or check '$terraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-Error "Can't delete cloud resources. Watch logs above or check '$terraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "timescale") {
    Remove-EnvMapValue -Map $resources -Key "timescale.private_ip"
    Remove-EnvMapValue -Map $resources -Key "timescale.public_ip"
    Remove-EnvMapValue -Map $resources -Key "timescale.id"
    Remove-EnvMapValue -Map $resources -Key "timescale.sg_id"
    Remove-EnvMapValue -Map $resources -Key "timescale.subnet_id"
    Remove-EnvMapValue -Map $resources -Key "timescale.keypair_name"
    Remove-EnvMapValue -Map $resources -Key "timescale.private_key_path"
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
