#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Check timescaleql version
if ((Get-EnvMapValue -Map $config -Key "timescale.version") -lt 11) {
    Write-Error "Use timescale version 11 or higher to install timescale db."
}

# Prepare hosts file
$ansibleInventory = @("[timescale]")
$ansibleInventory += "timescale ansible_host=$(Get-EnvMapValue -Map $resources -Key "timescale.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "timescale.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $resources -Key "timescale.private_key_path")"
Set-Content -Path "$PSScriptRoot/../temp/timescale_ansible_hosts" -Value $ansibleInventory

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/timescale_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/timescale_ansible_hosts" "$PSScriptRoot/templates/ssh_keyscan_playbook.yml"
}

# Read parameters
$version = Get-EnvMapValue -Map $config -Key "timescale.version"
$name = Get-EnvMapValue -Map $config -Key "timescale.name"
$username = Get-EnvMapValue -Map $config -Key "timescale.username"
$password = Get-EnvMapValue -Map $config -Key "timescale.password"

# Set template parameters
$templateParams = @{ 
    version=$version; 
    name=$name; 
    username=$username; 
    password=$password;
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/timescale_install_playbook.yml" -OutputPath "$PSScriptRoot/../temp/timescale_install_playbook.yml" -Params1 $templateParams

if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/timescale_ansible_hosts" "$PSScriptRoot/../temp/timescale_install_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/timescale_ansible_hosts" "$PSScriptRoot/../temp/timescale_install_playbook.yml"
}

if ($LastExitCode -ne 0) {
    Write-EnvError -Component "timescale" "Error on installing timescale. See logs above."
}

# Write timescale resources
$port = "5432"
$serviceHost = Get-EnvMapValue -Map $resources -Key "timescale.private_ip"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "timescale.port" -Value $port
Set-EnvMapValue -Map $resources -Key "timescale.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "timescale.endpoint" -Value $endpoint
Set-EnvMapValue -Map $resources -Key "timescale.inventory" -Value $ansibleInventory
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
