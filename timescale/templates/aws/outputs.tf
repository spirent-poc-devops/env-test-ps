# Save timescale instance id
output "timescale_id" {
  value       = module.timescale_instance.id
  description = "The unique identifier of the timescale virtual machine."
}

# Save timescale security group id
output "timescale_sg_id" {
  value       = aws_security_group.timescale.id
  description = "The unique identifier of the timescale security group."
}

# Save timescale instance private ip
output "timescale_private_ip" {
  value       = module.timescale_instance.private_ip
  description = "The private IP address of the timescale virtual machine."
}

# Save timescale instance public ip
output "timescale_public_ip" {
  value       = module.timescale_instance.public_ip
  description = "The public IP address of the timescale virtual machine."
}

# Save timescale instance public ip
output "timescale_keypair_name" {
  value       = module.ssh_key_pair.key_name
  description = "The public IP address of the timescale virtual machine."
}