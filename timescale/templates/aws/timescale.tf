# Configure AWS security group
resource "aws_security_group" "timescale" {
  name        = "timescale-${var.env_nameprefix}"
  description = "Timescale access"
  vpc_id      = var.vpc

  ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.mgmt_ip}/32"]
  }

  ingress {
    description = "access_to_timescale"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "timescale_${var.env_nameprefix}_sg"
  }
}

module "label" {
  source = "cloudposse/label/null"
  version = "0.24.1"
  name       = "${var.env_nameprefix}-timescale"
  delimiter  = "-"

  tags = {
    configprefix  = var.env_nameprefix
  }
}

module "ssh_key_pair" {
  source                = "cloudposse/key-pair/aws"
  version               = "0.18.0"
  namespace             = var.namespace
  stage                 = var.stage
  name                  = var.env_nameprefix
  ssh_public_key_path   = var.ssh_key_pair_path
  attributes            = ["timescale"]
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
  tags                  = module.label.tags
}

module "timescale_instance" {
  source                        = "cloudposse/ec2-instance/aws"
  version                       = "0.32.2"
  ami                           = var.ami
  ami_owner                     = var.ami_owner
  attributes                    = ["timescale-instance"]
  name                          = var.env_nameprefix
  create_default_security_group = true
  security_groups               = [aws_security_group.timescale.id]
  region                        = var.aws_region
  subnet                        = var.subnet
  vpc_id                        = var.vpc
  associate_public_ip_address   = var.associate_public_ip_address
  ssh_key_pair                  = module.ssh_key_pair.key_name
  assign_eip_address            = false
  instance_type                 = var.instance_type
  root_volume_size              = var.root_volume_size
  delete_on_termination         = true
  tags = merge(module.label.tags, map("confignameprefix",var.env_nameprefix),  map("Description", "Timescale Server"), map("OS", "Ubuntu Server 18.04 LTS"))
}

