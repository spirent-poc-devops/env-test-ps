#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare hosts file if missing
if (!(Test-Path -Path "$PSScriptRoot/../temp/timescale_ansible_hosts")) {
    $ansibleInventory = @("[timescale]")
    $ansibleInventory += "timescale ansible_host=$(Get-EnvMapValue -Map $resources -Key "timescale.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "timescale.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $resources -Key "timescale.private_key_path")"
    Set-Content -Path "$PSScriptRoot/../temp/timescale_ansible_hosts" -Value $ansibleInventory
}

# Destroy timescale database
$version = Get-EnvMapValue -Map $config -Key "timescale.version"

$templateParams = @{ 
    version=$version; 
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/timescale_uninstall_playbook.yml" -OutputPath "$PSScriptRoot/../temp/timescale_uninstall_playbook.yml" -Params1 $templateParams

if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$PSScriptRoot/../temp/timescale_ansible_hosts" "$PSScriptRoot/../temp/timescale_uninstall_playbook.yml"
} else {
    ansible-playbook -i "$PSScriptRoot/../temp/timescale_ansible_hosts" "$PSScriptRoot/../temp/timescale_uninstall_playbook.yml"
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "timescale") {
    Remove-EnvMapValue -Map $resources -Key "timescale.host"
    Remove-EnvMapValue -Map $resources -Key "timescale.endpoint"
    Remove-EnvMapValue -Map $resources -Key "timescale.port"
    Remove-EnvMapValue -Map $resources -Key "timescale.inventory"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
