#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

# $adminToken = -join ((48..57)+(97..122) |Get-Random -Count 32 |%{[char]$_})
# $viewerToken = -join ((48..57)+(97..122) |Get-Random -Count 32 |%{[char]$_})
$adminToken = "edd1c9f034335f136f87ad84b625c8f1"
$viewerToken = "4054f7cf07e344346cd3f287985e76a2"

$templateParams = @{
  namespace = $namespace
  apisix_etcd_replica_count = Get-EnvMapValue -Map $config -Key "apisix.etcd_replica_count"
  adminToken = $adminToken
  viewerToken = $viewerToken
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/values.yml" -OutputPath "$PSScriptRoot/../temp/apisix_values.yml" -Params1 $templateParams

# Create configmap for apisix customPlugins
kubectl apply -f "$PSScriptRoot/templates/apisix-prometheus-conf.yml"

# Todo: replace helm chart with a yml file to use private images
helm repo add apisix https://charts.apiseven.com
helm repo update
helm install apisix apisix/apisix `
  --values "$PSScriptRoot/../temp/apisix_values.yml" `
  --namespace $namespace

Set-EnvMapValue -Map $resources -Key "apisix.admin_token" -Value $adminToken
Set-EnvMapValue -Map $resources -Key "apisix.viewer_token" -Value $viewerToken
# use default tokens for ingress-controller
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

# Wait apisix to start
$startTime = Get-Date
$waitMinutes = 10
Write-Host "Waiting for apisix controller to run...`nUsually it takes about 5 mins. Script will wait maximum for $waitMinutes minutes while pods are initializing..."
do {
    Start-Sleep -Seconds 10
    $res = $(kubectl get pods -n $namespace -l app.kubernetes.io/instance=apisix --field-selector=status.phase!=Running)
    $now = Get-Date
} while ( ($res -ne $null) -and ($($now - $startTime).Minutes -lt $waitMinutes) );

# Create apisix dashboard
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/apisix-dashboard-conf.yml" -OutputPath "$PSScriptRoot/../temp/apisix-dashboard-conf.yml" -Params1 $templateParams
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/apisix-dashboard.yml" -OutputPath "$PSScriptRoot/../temp/apisix-dashboard.yml" -Params1 $templateParams

kubectl create -n $namespace configmap apisix-dashboard-config --from-file "$PSScriptRoot/../temp/apisix-dashboard-conf.yml"
kubectl apply -f "$PSScriptRoot/../temp/apisix-dashboard.yml"

# Record results and save them to disk
$port = $(kubectl get --namespace $namespace -o jsonpath="{.spec.ports[0].nodePort}" services apisix-gateway)
$adminPort = $(kubectl get --namespace $namespace -o jsonpath="{.spec.ports[0].nodePort}" services apisix-admin)
$serviceHost = "apisix-gateway." + $namespace + ".svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "apisix.state" -Value "deployed"
Set-EnvMapValue -Map $resources -Key "apisix.port" -Value $port
Set-EnvMapValue -Map $resources -Key "apisix.admin_port" -Value $adminPort
Set-EnvMapValue -Map $resources -Key "apisix.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "apisix.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

# # Enable prometheus plugin
# $k8sHostIp = Get-EnvMapValue -Map $resources -Key "k8s.public_ip"
# $apisixAdminToken = Get-EnvMapValue -Map $resources -Key "apisix.admin_token"
# $apisixAdminPort = Get-EnvMapValue -Map $resources -Key "apisix.admin_port"
# Write-Host "Enabling apisix prometheus plugin..."
# $postParams = @"
# {
#   "uri": "/hello",
#   "plugins": {
#       "prometheus":{}
#   }
# }
# "@
# $headers = @{
#     "X-API-KEY" = $apisixAdminToken;
#     "Content-Type" = "application/json";
# }
# $res = Invoke-WebRequest "$k8sHostIp`:$apisixAdminPort/apisix/admin/routes/0"`
#         -Headers $headers -Body $postParams -Method 'PUT'
