#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"

$templateParams = @{
  namespace = $namespace
  keycloak_username = Get-EnvMapValue -Map $config -Key "keycloak.username"
  keycloak_password = Get-EnvMapValue -Map $config -Key "keycloak.password"
  db_host = Get-EnvMapValue -Map $resources -Key "timescale.host"
  db_port = Get-EnvMapValue -Map $resources -Key "timescale.port"
  db_name = Get-EnvMapValue -Map $config -Key "timescale.name"
  db_username = Get-EnvMapValue -Map $config -Key "timescale.username"
  db_password = Get-EnvMapValue -Map $config -Key "timescale.password"
}

Build-EnvTemplate -InputPath "$PSScriptRoot/templates/values.yml" -OutputPath "$PSScriptRoot/../temp/keycloak_values.yml" -Params1 $templateParams

# Todo: replace helm chart with a yml file to use private images
helm repo add codecentric https://codecentric.github.io/helm-charts
helm install keycloak codecentric/keycloak `
  --values "$PSScriptRoot/../temp/keycloak_values.yml" `
  --namespace $namespace

Start-Sleep -Seconds 60

# Enable apisix plugin
$k8sHostIp = Get-EnvMapValue -Map $resources -Key "k8s.public_ip"
$keycloakPort = $(kubectl get --namespace $namespace -o jsonpath="{.spec.ports[0].nodePort}" services keycloak-http)
$apisixGWPort = Get-EnvMapValue -Map $resources -Key "apisix.port"
$apisixAdminPort = Get-EnvMapValue -Map $resources -Key "apisix.admin_port"
$apisixAdminToken = Get-EnvMapValue -Map $resources -Key "apisix.admin_token"

# Get keycloak admin token
Write-Host "Getting keykloak access_token..."
$postParams = @{
    username = Get-EnvMapValue -Map $config -Key "keycloak.username";
    password = Get-EnvMapValue -Map $config -Key "keycloak.password";
    client_id = "admin-cli";
    grant_type = "password";
    }
$res = Invoke-WebRequest -Uri "http://$k8sHostIp`:$keycloakPort/auth/realms/master/protocol/openid-connect/token" `
            -Method POST -Headers @{'content-type'="application/x-www-form-urlencoded"} -Body $postParams
$resJson = $res.Content | ConvertFrom-Json
$accessToken = $resJson.access_token
Write-Host "Keykloak access_token: $accessToken"

# Create apisix client
Write-Host "Creating apisix client in keycloak..."
$postParams = @"
{
  "clientId": "apisix",
  "id": "1",
  "name": "APISIX Auth",
  "description": "APISIX ingress controller",
  "enabled": true,
  "redirectUris": [
    "http://$k8sHostIp`:$apisixGWPort/auth"
  ],
  "clientAuthenticatorType": "client-secret",
  "bearerOnly": false,
  "consentRequired": false,
  "standardFlowEnabled": true,
  "implicitFlowEnabled": true,
  "directAccessGrantsEnabled": true,
  "serviceAccountsEnabled": true,
  "authorizationServicesEnabled": true,
  "authorizationSettings": {
    "scopes": [
      {
        "displayName": "View Scope",
        "name": "view"
      }
    ],
    "resources": [
      {
        "name": "global",
        "uris": [
          "/*"
        ],
        "ownerManagedAccess": true,
        "displayName": "Gloabal",
        "attributes": {
          
        },
        "scopes": [
          {
            "name": "view"
          }
        ]
      }
    ]
  },
  "publicClient": false,
  "frontchannelLogout": false,
  "protocol": "openid-connect",
  "attributes": {
    "id.token.as.detached.signature": "false",
    "saml.assertion.signature": "false",
    "saml.force.post.binding": "false",
    "saml.multivalued.roles": "false",
    "saml.encrypt": "false",
    "token.endpoint.auth.signing.alg": "RS256",
    "oauth2.device.authorization.grant.enabled": "true",
    "backchannel.logout.revoke.offline.tokens": "false",
    "saml.server.signature": "false",
    "saml.server.signature.keyinfo.ext": "false",
    "use.refresh.tokens": "true",
    "exclude.session.state.from.auth.response": "false",
    "oidc.ciba.grant.enabled": "false",
    "saml.artifact.binding": "false",
    "backchannel.logout.session.required": "true",
    "client_credentials.use_refresh_token": "false",
    "saml_force_name_id_format": "false",
    "require.pushed.authorization.requests": "false",
    "saml.client.signature": "false",
    "tls.client.certificate.bound.access.tokens": "false",
    "saml.authnstatement": "false",
    "display.on.consent.screen": "false",
    "saml.onetimeuse.condition": "false"
  },
  "defaultClientScopes": [
    "web-origins",
    "view",
    "roles",
    "profile",
    "email"
  ],
  "optionalClientScopes": [
    "address",
    "phone",
    "offline_access",
    "microprofile-jwt"
  ],
  "access": {
    "view": true,
    "configure": true,
    "manage": true
  }
}
"@
$headers = @{
    "Authorization" = "Bearer $accessToken";
    "Content-Type" = "application/json";
}
$res = Invoke-WebRequest -Uri "http://$k8sHostIp`:$keycloakPort/auth/admin/realms/master/clients" `
            -Method POST -Headers $headers -Body $postParams
if ($res.StatusCode -eq 201) {
    Write-Host "Apisix client created"
} else {
    Write-EnvError -Component "keycloak" "Error creating apisix client."
}

# Get client_secret 
Write-Host "Get apisix client_secret..."
$headers = @{
    "Authorization" = "Bearer $accessToken";
}
$res = Invoke-WebRequest -Uri "http://$k8sHostIp`:$keycloakPort/auth/admin/realms/master/clients/1/client-secret" `
            -Method GET -Headers $headers
$resJson = $res.Content | ConvertFrom-Json
$client_secret = $resJson.value
Write-Host "Apisix client_secret: $client_secret"
Set-EnvMapValue -Map $resources -Key "keycloak.client_secret" -Value $client_secret

# Enable keycloak plugin in apisix
Write-Host "Enabling apisix keycloak plugin..."
$postParams = @"
{
    "plugins": {
        "authz-keycloak": {
            "token_endpoint": "http://$k8sHostIp`:$keycloakPort/auth/realms/master/protocol/openid-connect/token",
            "permissions": ["global#view"],
            "client_id": "apisix",
            "client_secret":"$client_secret"
        }
    }
}
"@
$headers = @{
    "X-API-KEY" = $apisixAdminToken;
    "Content-Type" = "application/json";
}
$res = Invoke-WebRequest "$k8sHostIp`:$apisixAdminPort/apisix/admin/global_rules/1"`
        -Headers $headers -Body $postParams -Method 'PUT'

# Record results and save them to disk
$serviceHost = "keycloak-http." + $namespace + ".svc.cluster.local"
$endpoint = "$($serviceHost):$($keycloakPort)"

Set-EnvMapValue -Map $resources -Key "keycloak.state" -Value "deployed"
Set-EnvMapValue -Map $resources -Key "keycloak.port" -Value $keycloakPort
Set-EnvMapValue -Map $resources -Key "keycloak.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "keycloak.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
