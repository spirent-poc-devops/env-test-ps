#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

# Create k8s component
kubectl apply -f "$PSScriptRoot/../temp/neo4j.yml"

# Record results and save them to disk
$port = kubectl get svc neo4j -n $namespace -o=jsonpath="{.spec.ports[0].targetPort}"
$boltPort = kubectl get svc neo4j -n $namespace -o=jsonpath="{.spec.ports[1].targetPort}"
$serviceHost = "neo4j.$namespace.svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "neo4j.port" -Value $port
Set-EnvMapValue -Map $resources -Key "neo4j.bolt_port" -Value $boltPort
Set-EnvMapValue -Map $resources -Key "neo4j.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "neo4j.endpoint" -Value $endpoint
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
