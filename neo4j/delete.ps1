#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

kubectl delete --ignore-not-found=true -f "$PSScriptRoot/../temp/neo4j.yml"

if ($LastExitCode -ne 0){
    Write-EnvError -Component "neo4j" "There were errors deleting neo4j, Watch logs above"
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "neo4j") {
    Remove-EnvMapValue -Map $resources -Key "neo4j.port"
    Remove-EnvMapValue -Map $resources -Key "neo4j.bolt_port"
    Remove-EnvMapValue -Map $resources -Key "neo4j.host"
    Remove-EnvMapValue -Map $resources -Key "neo4j.endpoint"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
