# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> Spirent VisionWorks Test Environment

This is the environment automation scripts to create, update and delete Test Environments. The Test environments are functionally identical
to production environments, but require less resources and more economical to run in numbers by development team. The environment is primarily
used for automated continuous integration and manual testing. For non-functional testing it is recommended to temporary create production environment
and delete it after the tests.

The environment consists of the following components:
* **Kubernetes** cluster
* **Timescale** database
* **Neo4j** database
* **Kafka** message broker
* **Redis** distributed cache
* **ElasticSearch + Kibana** consolidated logging
* **Prometheus + Grafana** consolidated metrics

<img src="design.png" width="800">

<a name="links"></a> Quick links:

* [Max Infrastructure Requirements](https://nowhere.com)
* [Max Infrastructure Architecture](https://nowhere.com)
* [Change Log](CHANGELOG.md)

## Use

Start the process from any computer. Download released package with environment provisioning scripts from [link...](http://somewhere.com/env-test-ps-1.0.0.zip)
```bash
wget http://somewhere.com/env-test-ps-1.0.0.zip
```

Unzip scripts from the package
```bash
unzip env-test-ps-1.0.0.zip
```

Go to the `config` folder and create configuration for a new environment.
Read configuration section below and use [default_config.json](config/default_config.json)
as your starting point.

Create the management station, that will be used to create new environments.
```bash
create_mgmt_station.ps1 -Config <path to config file>
```

After you run the script you shall see a resource file (ending with `resources.json`) next to the config file you created.

Connect to the newly created management station (see it's address and credentials in the script output).
Install the environment scripts following the steps above and copy into `config` folder on the management station
config and resource files from your local computer. **The following steps shall be performed from the management station**.

Install prerequisites on the management station by running the installation script for the corresponding platform.
```bash
install_prereq_[win | mac | ubuntu].[ps1 | sh]
```

Create a new environment. The resources file will contain references to newly created resources.
```bash
create_env.ps1 -Config <path to config file>
```

When environment configuration changes, you can update it without loosing data or reinstalling applications.
```bash
update_env.ps1 -Config <path to config file>
```

Delete the environment when its no longer needed to free up used computing resources.
```bash
delete_env.ps1 -Config <path to config file>
```

## Configuration

The environment configuration supports the following configuration parameters:

<!-- Todo -->
<table>
<tr><th>Parameter</th><th>Type</th><th>Default</th><th>Description</th></tr>
<tr>
<td>env.type</td><td>string</td><td>sample</td><td>Type of the created environment</td>
</tr>
</table>

Your configuration may look like the sample below.
```json
{
    "environment":{
        "type": "test",
        "name": "testenv-demo",
        "nameprefix": "testenv",
        "version": "1.0.0"
    },
    "aws":{
        "region": "us-east-1",
        "account_id": "072434860318",
        "terraform_role_name": "spirent-sbx006-admin",
        "namespace" : "sbx006",
        "stage" : "sandbox",
        "stage_short" : "s",
        "vpc": "vpc-09e59cb6c44b1c5c4"
    },
    "mgmtstation": {
        "instance_ami": "ami-08cc10a3eebe46341",
        "instance_type": "t3.medium",
        "root_volume_size": "50",
        "instance_username": "ubuntu",
        "subnet": "subnet-028bb6b6d2ca7342e",
        "sg_id": "sg-0ad956e2f350f92e7"
    },
    "copy_project_to_mgmt_station" : true,
    "k8s": {
        "version": "1.18.5",
        "s3_store": "s3://",
        "dns_zone": "spirentsampleapppock8s.sbx006.spirent.io",
        "master_zones": [ "us-east-1a" ],
        "node_zones": [ "us-east-1a","us-east-1b","us-east-1c" ],
        "master_count": 1,
        "node_count": 3,
        "master_instance_type": "m4.xlarge",
        "node_instance_type": "t3.medium",
        "ami": "ami-07d74e8c762e90630",
        "keypair_name": "~/.ssh/id_rsa.pub",
        "network_cidr": "172.31.0.0/16",
        "namespace": "infra",
        "subnets": [ "subnet-028bb6b6d2ca7342e","subnet-0c637d6329c805b3b","subnet-0c331448bb9cb9912" ],
        "kube_config_file_path" : "~/.kube/config"
    },
    "kafka":{
        "replica":3
    },
    "timescale":{
        "cloud": "aws",
        "username": "ubuntu",
        "vm_username": "ubuntu",
        "subnet": "subnet-028bb6b6d2ca7342e",
        "subnet_zone": "us-east-1a",
        "subnet_cidr": "172.31.10.0/24",
        "instance_type": "t2.medium",
        "instance_ami": "ami-056db1277deef2218",
        "instance_ami_owner": "099720109477",
        "version": 11,
        "name": "spirentstage",
        "password": "SPIRENTadmin2021#",
        "root_volume_size": "50"
    },
    "route53":{
        "hosted_zone_id":"Z0880600MVJB229J5CWW"
    },
    "docker" : { 
        "registry" : "ghcr.io", 
        "username" : "XXXXX", 
        "password" : "XXXXX", 
        "email" : "XXX.XXX@spirent.com" 
    }
}
```

## Resources

As the result of the scripts information about created resources will be saved into a resource file that is ended with `resources.json`
and placed next to the configuration file used to create an environment.

Resource parameters saved for the sample component:
<!-- Todo -->
<table>
<tr><th>Parameter</th><th>Type</th><th>Description</th></tr>
<tr>
<td>component.output_1</td><td>string</td><td>The first output</td>
</tr>
<tr>
<td>component.output_2</td><td>string</td><td>The second output</td>
</tr>
</table>

Example of the environment's resources file:
```json
{
  "environment": {
    "create_time": "2021-03-26T19:25:59.9156544+00:00",
    "version": "1.0.0",
    "name": "mm_testenv",
    "type": "test"
  },
  "k8s": {
    "node_instance_type": "t3.medium",
    "cluster_name": "testenv-spirentsampleapppock8s.sbx006.spirent.io",
    "master_instance_type": "m4.xlarge",
    "keypair_name": "~/.ssh/id_rsa.pub",
    "version": "1.18.5",
    "dns_zone": "spirentsampleapppock8s.sbx006.spirent.io",
    "ami": "ami-07d74e8c762e90630",
    "s3_name": "s3://sbx006-sandbox-testenv",
    "master_zones": "us-east-1a",
    "network_cidr": "172.31.0.0/16",
    "namespace": "infra",
    "subnets": [
      "subnet-028bb6b6d2ca7342e",
      "subnet-0c637d6329c805b3b",
      "subnet-0c331448bb9cb9912"
    ],
    "node_count": 3
  },
  "timescale": {
    "sg_id": "sg-02fa0c0675e46e8ec",
    "private_ip": "172.31.1.12",
    "public_ip": "54.242.110.126",
    "endpoint": "172.31.1.12:5432",
    "port": "5432",
    "id": "i-0874351dec88b94e3",
    "inventory": [
      "[timescale]",
      "timescale ansible_host=172.31.1.12 ansible_ssh_user=ubuntu ansible_ssh_private_key_file=temp/timescale_env-test/secrets/pipdevs-sandbox-testenv-timescale.pem"
    ]
  },
  "mgmtstation": {
    "ssh_command": "ssh -i \"mgmt_station/terraform_scripts/secrets/sbx006-sandbox-testenv-mgmt-station.pem\" ubuntu@3.94.158.110",
    "KeyName": "sbx006-sandbox-testenv-mgmt-station",
    "InstanceName": "sbx006-sandbox-testenv-mgmt-station",
    "private_ip": "172.31.1.29",
    "public_ip": "3.92.56.193",
    "envprefix": "testenv",
    "PublicIP": "3.92.56.193"
  },
  "route53": {
    "name": "route53",
    "sampleapp_url": "sampleapp.testenvspirentsampleapppock8s.sbx006.spirent.io"
  }
}
```

## Contacts

This environment was created and currently maintained by the team managed by *Sudheer Sangunni* .
