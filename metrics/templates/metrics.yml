apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: prometheus
rules:
- apiGroups: [""]
  resources:
  - nodes
  - nodes/proxy
  - services
  - endpoints
  - pods
  verbs: ["get", "list", "watch"]
- apiGroups:
  - extensions
  resources:
  - ingresses
  verbs: ["get", "list", "watch"]
- nonResourceURLs: ["/metrics"]
  verbs: ["get"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: prometheus
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: prometheus
subjects:
- kind: ServiceAccount
  name: default
  namespace: <%= namespace %>
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: prometheus-server-conf
  labels:
    name: prometheus-server-conf
  namespace: <%= namespace %>
data:
  prometheus.yml: |-
    global:
      scrape_interval: 5s
      evaluation_interval: 5s

    scrape_configs:
      - job_name: 'apisix'
        scrape_interval: 15s # This value will be related to the time range of the rate function in Prometheus QL. The time range in the rate function should be at least twice this value.
        metrics_path: '/apisix/prometheus/metrics'
        static_configs:
        - targets: ['<%= apisix_pod_ip %>:9091']

      # - job_name: 'kubernetes-apiservers'

      #   kubernetes_sd_configs:
      #   - role: endpoints
      #   scheme: https

      #   tls_config:
      #     ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      #     insecure_skip_verify: true
      #   bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

      #   relabel_configs:
      #   - source_labels: [__meta_kubernetes_namespace, __meta_kubernetes_service_name, __meta_kubernetes_endpoint_port_name]
      #     action: keep
      #     regex: default;kubernetes;https

      - job_name: 'kubernetes-nodes'

        scheme: https

        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

        kubernetes_sd_configs:
        - role: node

        relabel_configs:
        - action: labelmap
          regex: __meta_kubernetes_node_label_(.+)
        - target_label: __address__
          replacement: kubernetes.default.svc:443
        - source_labels: [__meta_kubernetes_node_name]
          regex: (.+)
          target_label: __metrics_path__
          replacement: /api/v1/nodes/${1}/proxy/metrics

      # - job_name: 'kubernetes-pods'

      #   kubernetes_sd_configs:
      #   - role: pod

      #   relabel_configs:
      #   - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_scrape]
      #     action: keep
      #     regex: true
      #   - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_path]
      #     action: replace
      #     target_label: __metrics_path__
      #     regex: (.+)
      #   - source_labels: [__address__, __meta_kubernetes_pod_annotation_prometheus_io_port]
      #     action: replace
      #     regex: ([^:]+)(?::\d+)?;(\d+)
      #     replacement: $1:$2
      #     target_label: __address__
      #   - action: labelmap
      #     regex: __meta_kubernetes_pod_label_(.+)
      #   - source_labels: [__meta_kubernetes_namespace]
      #     action: replace
      #     target_label: kubernetes_namespace
      #   - source_labels: [__meta_kubernetes_pod_name]
      #     action: replace
      #     target_label: kubernetes_pod_name

      - job_name: 'kubernetes-cadvisor'

        scheme: https

        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

        kubernetes_sd_configs:
        - role: node

        relabel_configs:
        - action: labelmap
          regex: __meta_kubernetes_node_label_(.+)
        - target_label: __address__
          replacement: kubernetes.default.svc:443
        - source_labels: [__meta_kubernetes_node_name]
          regex: (.+)
          target_label: __metrics_path__
          replacement: /api/v1/nodes/${1}/proxy/metrics/cadvisor
      
      # - job_name: 'kubernetes-service-endpoints'

      #   kubernetes_sd_configs:
      #   - role: endpoints

      #   relabel_configs:
      #   - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_scrape]
      #     action: keep
      #     regex: true
      #   - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_scheme]
      #     action: replace
      #     target_label: __scheme__
      #     regex: (https?)
      #   - source_labels: [__meta_kubernetes_service_annotation_prometheus_io_path]
      #     action: replace
      #     target_label: __metrics_path__
      #     regex: (.+)
      #   - source_labels: [__address__, __meta_kubernetes_service_annotation_prometheus_io_port]
      #     action: replace
      #     target_label: __address__
      #     regex: ([^:]+)(?::\d+)?;(\d+)
      #     replacement: $1:$2
      #   - action: labelmap
      #     regex: __meta_kubernetes_service_label_(.+)
      #   - source_labels: [__meta_kubernetes_namespace]
      #     action: replace
      #     target_label: kubernetes_namespace
      #   - source_labels: [__meta_kubernetes_service_name]
      #     action: replace
      #     target_label: kubernetes_name
      
      - job_name: 'pushgateway'

        kubernetes_sd_configs:
        - role: pod
        
        relabel_configs:
          - source_labels: [__meta_kubernetes_pod_label_app]
            regex: pushgateway
            action: keep

      - job_name: 'prometheus'

        kubernetes_sd_configs:
        - role: pod
        
        relabel_configs:
          - source_labels: [__meta_kubernetes_pod_label_app]
            regex: prometheus-server
            action: keep

      - job_name: 'grafana'

        kubernetes_sd_configs:
        - role: pod
        
        relabel_configs:
          - source_labels: [__meta_kubernetes_pod_label_name]
            regex: grafana
            action: keep

      - job_name: 'controller-facade'

        kubernetes_sd_configs:
        - role: pod
        
        relabel_configs:
          - source_labels: [__meta_kubernetes_pod_container_name]
            regex: controller-facade-pod
            action: keep

      # - job_name: '<%= namespace %>-pods'

      #   kubernetes_sd_configs:
      #   - role: pod
        
      #   relabel_configs:
      #     - source_labels: [__meta_kubernetes_namespace]
      #       regex: <%= namespace %>
      #       action: keep
      #     - source_labels: [__address__]
      #       action: replace
      #       target_label: __address__
      #       regex: ([^:]+)(?::\d+)?;(\d+)
      #       replacement: $1:8080
---
apiVersion: v1
kind: Service
metadata:
  name: prometheus
  namespace: <%= namespace %>
spec:
  selector: 
    app: prometheus-server
  ports:
  - port: 9090
    protocol: TCP
    targetPort: 9090 
    nodePort: 30090
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: prometheus-deployment
  namespace: <%= namespace %>
spec:
  replicas: 1
  selector:
    matchLabels:
      app: prometheus-server
  template:
    metadata:
      labels:
        app: prometheus-server
    spec:
      containers:
        - name: prometheus
          image: artifactory.vwx.spirent.com/dockerhub/prom/prometheus:latest
          # image: prom/prometheus:latest
          args:
            - "--config.file=/etc/prometheus/prometheus.yml"
            - "--storage.tsdb.path=/prometheus/"
          ports:
            - containerPort: 9090
          volumeMounts:
            - name: prometheus-config-volume
              mountPath: /etc/prometheus/
            - name: prometheus-storage-volume
              mountPath: /prometheus/
      volumes:
        - name: prometheus-config-volume
          configMap:
            defaultMode: 420
            name: prometheus-server-conf
        - name: prometheus-storage-volume
          emptyDir: {}
---
apiVersion: v1
kind: Service
metadata:
  name: grafana
  namespace: <%= namespace %>
spec:
  ports:
  - nodePort: 32300
    port: 3000
    protocol: TCP
    targetPort: 3000
  selector:
    name: grafana
  type: NodePort
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: grafana-provisioning-pv
  namespace: <%= namespace %>
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 2Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/grafana"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: grafana-data-pv
  namespace: <%= namespace %>
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 2Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/grafana-data"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  namespace: <%= namespace %>
  name: grafana-provisioning-pv-claim
spec:
  volumeName: grafana-provisioning-pv
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  namespace: <%= namespace %>
  name: grafana-data-pv-claim
spec:
  volumeName: grafana-data-pv
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: grafana
  name: grafana-deployment
  namespace: <%= namespace %>
spec:
  replicas: 1
  selector:
    matchLabels:
      name: grafana
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        name: grafana
    spec:
      containers:
      # - image: izakmarais/grafana-reporter:latest
      #   imagePullPolicy: IfNotPresent
      #   name: reporter
      #   args: ["-ip", "localhost:3000", "-grid-layout", "1"]
      #   ports:
      #   - containerPort: 8686
      #     protocol: TCP
      # - image: grafana/grafana-image-renderer:latest
      #   imagePullPolicy: IfNotPresent
      #   name: grafana-image-rendering
      #   ports:
      #   - containerPort: 8081
      #     protocol: TCP
      #   resources:
      #     limits:
      #       cpu: 500m
      #       memory: 2500Mi
      #     requests:
      #       cpu: 100m
      #       memory: 100Mi
      - name: grafana
        image: artifactory.vwx.spirent.com/dockerhub/grafana/grafana:<%= grafana_version %>
        # image: grafana/grafana:<%= grafana_version %>
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 3000
          protocol: TCP
        env:
        # - name: GF_PATHS_CONFIG
        #   value: "/etc/grafana/provisioning/grafana.ini"
        # - name: GF_RENDERING_SERVER_URL
        #   value: "http://localhost:8081/render"
        # - name: GF_RENDERING_CALLBACK_URL
        #   value: "http://localhost:3000/"
        - name: GF_INSTALL_PLUGINS
          value: "grafana-simple-json-datasource 1.4.1,simpod-json-datasource 0.2.3"
        # - name: GF_DATAPROXY_TIMEOUT
        #   value: "300"
        # - name: GF_RENDERING_CONCURRENT_RENDER_REQUEST_LIMIT
        #   value: "5"
        # - name: GF_ALERTING_CONCURRENT_RENDER_LIMIT
        #   value: "1"
        resources:
          limits:
            cpu: 500m
            memory: 2500Mi
          requests:
            cpu: 100m
            memory: 100Mi
        volumeMounts:
        - name: provisioning
          mountPath: /etc/grafana/provisioning
        - name: grafana-data
          mountPath: /var/lib/grafana
      restartPolicy: Always
      securityContext: {}
      terminationGracePeriodSeconds: 30
      initContainers:
      - name: grafana-init
        image: artifactory.vwx.spirent.com/dockerhub/mohammadim/bussybox:latest
        # image: busybox
        command: ["/bin/sh","-c"]
        args: ["chown -R 472:472 /data; touch /data/grafana.ini"] # 472 grafana user 
        volumeMounts:
        - name: provisioning
          mountPath: /data
      - name: grafana-data-init
        image: artifactory.vwx.spirent.com/dockerhub/mohammadim/bussybox:latest
        # image: busybox
        command: ["/bin/sh","-c"]
        args: ["chown -R 472:472 /data"] # 472 grafana user 
        volumeMounts:
        - name: grafana-data
          mountPath: /data
      volumes:
      - name: provisioning
        persistentVolumeClaim:
          claimName: grafana-provisioning-pv-claim
      - name: grafana-data
        persistentVolumeClaim:
          claimName: grafana-data-pv-claim
---
apiVersion: v1
kind: Service
metadata:
  name: pushgateway-metrics
  namespace: <%= namespace %>
spec:
  ports:
  - port: 9091
    protocol: TCP
    targetPort: 9091
  selector:
    name: pushgateway
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pushgateway-deployment
  namespace: <%= namespace %>
spec:
  replicas: 1
  selector:
    matchLabels:
      app: pushgateway
  template:
    metadata:
      labels:
        app: pushgateway
    spec:
      containers:
        - name: pushgateway
          image: artifactory.vwx.spirent.com/dockerhub/prom/pushgateway:latest
          # image: prom/pushgateway:latest
          ports:
            - containerPort: 9091
