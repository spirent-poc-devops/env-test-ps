#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

# Read parameters
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$grafanaVersion = Get-EnvMapValue -Map $config -Key "metrics.grafana_version"
$apisixPodName = $(kubectl get pod -l app.kubernetes.io/name=apisix -n $namespace -o jsonpath="{.items[0].metadata.name}")
$apisixPodIp = $(kubectl get pod $apisixPodName -n $namespace --template={{.status.podIP}})

$templateParams = @{ 
    namespace=$namespace;
    grafana_version=$grafanaVersion;
    apisix_pod_ip = $apisixPodIp;
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/metrics.yml" -OutputPath "$PSScriptRoot/../temp/metrics.yml" -Params1 $templateParams
