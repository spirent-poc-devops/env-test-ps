#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

# Create k8s component
kubectl apply -f "$PSScriptRoot/../temp/metrics.yml"

# Record results and save them to disk
$port = kubectl get svc grafana -n $namespace -o=jsonpath="{.spec.ports[0].targetPort}"
$serviceHost = "grafana.$namespace.svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "metrics.port" -Value $port
Set-EnvMapValue -Map $resources -Key "metrics.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "metrics.endpoint" -Value $endpoint
Set-EnvMapValue -Map $resources -Key "metrics.grafana_version" -Value $grafana_version
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
