#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

kubectl delete --ignore-not-found=true -f "$PSScriptRoot/../temp/logging.yml"

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "logging") {
    Remove-EnvMapValue -Map $resources -Key "logging.port"
    Remove-EnvMapValue -Map $resources -Key "logging.host"
    Remove-EnvMapValue -Map $resources -Key "logging.endpoint"
    Remove-EnvMapValue -Map $resources -Key "logging.elastisearch_version"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
