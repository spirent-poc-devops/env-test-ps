#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Generate template
. "$PSScriptRoot/templates.ps1" -Config $config -Resources $resources

# Create k8s component
kubectl apply -f "$PSScriptRoot/../temp/logging.yml"

# Record results and save them to disk
$port = kubectl get svc kibana-logging -n $namespace -o=jsonpath="{.spec.ports[0].port}"
$serviceHost = "kibana-logging.$namespace.svc.cluster.local"
$endpoint = "$($serviceHost):$($port)"

Set-EnvMapValue -Map $resources -Key "logging.port" -Value $port
Set-EnvMapValue -Map $resources -Key "logging.host" -Value $serviceHost
Set-EnvMapValue -Map $resources -Key "logging.endpoint" -Value $endpoint
Set-EnvMapValue -Map $resources -Key "logging.elastisearch_version" -Value $elastisearch_version
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

# Wait for volumes to bound
Start-Sleep -Seconds 30

