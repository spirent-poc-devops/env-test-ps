#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

# Read parameters
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$elastisearch_version = Get-EnvMapValue -Map $config -Key "logging.elastisearch_version"

$templateParams = @{ 
    namespace=$namespace;
    elastisearch_version=$elastisearch_version;
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/logging.yml" -OutputPath "$PSScriptRoot/../temp/logging.yml" -Params1 $templateParams
